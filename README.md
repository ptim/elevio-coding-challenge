# Elev.io Coding Challenge

Deployed at: https://ptim-elevio-search-app-808f4b.netlify.app/

Please refer to the [documentation folder](https://gitlab.com/ptim/elevio-coding-challenge/tree/master/documentation), where you can see the planning and ideation process that contributed to the outcome:

- UI flow shorthand
- Design process with moodboard and toy logo

I opted to focus on delivering a mobile app, but the docs contain plans for a tablet-style layout for larger devices.

## Installing

- check out the code
- `yarn && yarn start`
- `yarn test`

## Requirements

### Functional Requirements

- on enter or by pressing the submit button, the current text entered in the search input should be submitted to our REST API to fetch a list of matching articles
- to be submitted, there must be a minimum of **three** characters in the search input
- the results of the search should be shown on the page
- on clicking the search result, the user will be navigated to a page to display the article (don't create the page to display the article but put in a mechanism to forward the user to a new page)
- also, we would like to track both the search and the clicking of the search result; put in place a mechanism to send events off to an API
- submit the code and documentation via GitHub (don't squash your commits)

### Non-Functional Requirements

- use the Elevio REST API to get a list of matching articles
- handle loading, error and success states
- allow the user to clear the input by pressing some sort of clear button
- deal with limiting the number of search results displayed
- ensure the code is testable and maintainable

### Bonus

- accessibility
- testing
- functional programming
- design, UX
- Typescript
- DX
- iconography & animations

## Design Desicions

### useReducer, useEffect

I took this as an opportunity to explore react hooks more deeply rather than use redux.

At present, it seems there is not an clear pattern for fetching / running side effects alongside
`useReducer`, where we would otherwise use thunks or sagas with redux. I saw several options
suggested, and opted for RWieruch's model, because it didn't involve tools other than hooks.

@dan_abramov replying to @antoniostoilkov, Mar 9:

> ...we’re still figuring this out and you should experiment with which patterns work best in practice.

- [Async Operations with useReducer Hook](https://gist.github.com/astoilkov/013c513e33fe95fa8846348038d8fe42)
- [nathanbuchar/react-hook-thunk-reducer](https://github.com/nathanbuchar/react-hook-thunk-reducer)
- [How to fetch data with React Hooks? - RWieruch](https://www.robinwieruch.de/react-hooks-fetch-data/)

## TODOs

I haven't implemented pagination, though I am limiting results from the API.

Had time allowed, I would have:

- added animations to support the user's interactions, and
- tablet and desktop views (the content is a bit sparse!)
- focus form input on load
- infinite scrolling of paged results
- keyboard shortcuts! `/` to focus search, `Escape` to clear the input, `?` to see help

```
Given the search field is blank
Then the reset button should be hidden

Given the search field contains text
Then the Reset button should be visible

Given the user has entered a valid search term
When they click the Search button
Then the Loading component should display

Given results are returned
When they have loaded
Then the Loading component should disappear
And the Search button should animate out (quickly fade out as it translates up)
And the results should animate in a staggered fashion

Given results are showing and the search button is hidden
When the user clicks the Reset button or enters the search field
Then the Search button should animate in (quickly fade in as it translates down)
And existing results should be pushed down the page
```

## Credits

- [Photo by Juan Davila on Unsplash](https://unsplash.com/photos/P8PlK2nGwqA)
