export const dummyResponse = {
  queryTerm: 'cat',
  totalResults: 3,
  totalPages: 1,
  currentPage: 1,
  count: 3,
  results: [
    {
      id: '14',
      title: 'Howling',
      category_id: 5,
    },
    {
      id: '3',
      title: 'Dorian',
      category_id: 2,
    },
    {
      id: '6',
      title: 'Riverside',
      category_id: 2,
    },
  ],
}

export const dummyState = {
  error: null,
  isSubmitting: false,
  hasSubmitted: true,
  isLoading: false,
  // response: {
  //   count: 0,
  //   currentPage: 0,
  //   results: [],
  //   totalPages: 0,
  //   totalResults: 0,
  // },
  response: dummyResponse,
  term: 'cat',
}
