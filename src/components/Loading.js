import React from 'react'

import './Loading.css'

const Loading = () => {
  return (
    <div className="flex justify-center mt-12">
      {/* Courtesy of https://loading.io/css/ */}
      <div className="lds-roller" title="loading" role="status">
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
        <div />
      </div>
    </div>
  )
}

export default Loading
