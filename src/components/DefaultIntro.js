import React from 'react'

import { actionCreators } from '../utils/reducer'
const { TERM_SUBMITTED } = actionCreators

const handleClick = (dispatch, term) => event => {
  event.preventDefault()
  dispatch({ type: TERM_SUBMITTED, payload: term })
}

const DefaultIntro = ({ dispatch, teaserText }) => {
  const suggestions = ['fog all around us', 'winter came', 'your spell']
  return (
    <>
      <p>{teaserText || 'Brooding, poignant:'}</p>
      <ul className="ml-6 mb-6">
        {suggestions.map(term => (
          <li key={term} className="list-disc">
            <a
              href={`/?search=${term}`}
              className="text-lg text-white italic no-underline"
              onClick={handleClick(dispatch, term)}
            >
              "<span className="underline">{term}</span>"
            </a>
          </li>
        ))}
      </ul>
    </>
  )
}

export default DefaultIntro
