import React from 'react'
import { withFormik, Form, Field, ErrorMessage } from 'formik'

import { ReactComponent as ResetIcon } from '../assets/reset-icon.svg'

import { actionCreators } from '../utils/reducer'
const { TERM_SUBMITTED, TERM_RESET, TERM_UPDATED } = actionCreators
/*
# Notes

We use a native form element to preserve accessibility.

We avoid using a input[type=search] due to limited browser support for the augmentation,
and the added requirements to provide consistent styling.

TODO: focus the search input onLoad
*/

const handleReset = dispatch => event => {
  event.preventDefault()
  dispatch({ type: TERM_RESET })
  // update history so that the user can user native navigation
  window.history.pushState({}, document.title, '/')
}

const wrappedHandleChange = (dispatch, handleChange) => event => {
  dispatch({ type: TERM_UPDATED, payload: event.target.value })
  return handleChange(event)
}

const SearchForm = ({ state, dispatch, handleChange }) => {
  // console.log(state);

  return (
    <Form className="relative mb-6">
      <Field
        id="search"
        type="text"
        name="search"
        className="
          w-full h-12 mb-6
          bg-white-80 text-gray-600
          placeholder-theme rounded-full shadow-inner-dark
          text-xl text-center italic
        "
        placeholder={`"Moody Lyrics"`}
        onChange={wrappedHandleChange(dispatch, handleChange)}
      />

      <button
        type="submit"
        className="
          w-full h-12 mb-6
          rounded-full
          bg-blue-gradient shadow-lg-dark
          text-white text-xl text-center
          anticipate-click depress-on-click
        "
      >
        Search
      </button>

      <a
        href="/"
        className="
          absolute top-0 right-0
          mr-3 mt-3
          text-gray-800
          transition transparent
        "
        style={!!state.term ? { opacity: 1, pointerEvents: 'auto' } : {}}
        onClick={handleReset(dispatch)}
        tabIndex="-1"
      >
        <ResetIcon title="Reset Search" className="w-6 h-6" />
      </a>

      <ErrorMessage
        name="search"
        component="div"
        className="p-4 bg-red-300 text-red-900 text-center"
      />
    </Form>
  )
}

const EnhancedSearchForm = withFormik({
  mapPropsToValues: ({ state }) => ({ search: state.term }),
  enableReinitialize: true,
  validate: values => {
    let errors = {}
    if (!values.search) {
      errors.search = 'What should we search for?'
      return errors
    }
    if (values.search.length < 3) {
      errors.search = 'Please enter at least three characters'
    }
    return errors
  },

  handleSubmit: (values, { props: { dispatch } }) => {
    dispatch({ type: TERM_SUBMITTED, payload: values.search })
  },
  displayName: 'EnhancedSearchForm',
})(SearchForm)
export default EnhancedSearchForm
