import React from 'react'

import { BASE_URL } from '../const'

import ben_howard from '../assets/ben_howard.jpg'
import agnes_obel from '../assets/agnes_obel.jpg'
import eefje_de_visser from '../assets/eefje_de_visser.jpg'
import ry_x from '../assets/ry_x.jpg'
import apparat from '../assets/apparat.jpg'

const ARTISTS_MAP = {
  1: 'Ben Howard',
  2: 'Agnes Obel',
  3: 'Eefje de Visser',
  4: 'Apparat',
  5: 'Ry X',
}

const images = {
  1: ben_howard,
  2: agnes_obel,
  3: eefje_de_visser,
  4: apparat,
  5: ry_x,
}

/*
Result.title contains HTML markup:
The `<em>search term</em>` is emphasized.
*/

const handleClick = event => {
  window.dataLayer = window.dataLayer || []
  window.dataLayer.push({
    event: 'search_open',
    article: event.currentTarget.href,
  })
}

function createMarkup(input) {
  return { __html: input }
}

const ResultsListItem = ({ result: { id, title, category_id } }) => {
  return (
    <li className="mb-6" onClick={handleClick}>
      <a
        href={`${BASE_URL}/articles/${id}`}
        className="
          flex items-start
          bg-white-20 shadow-lg
          text-white
          no-underline no-focus
        "
        onClick={handleClick}
        //target="_blank"
        //rel="noreferrer noopener"
      >
        <img
          src={images[category_id]}
          className="w-1/3"
          alt={ARTISTS_MAP[category_id]}
        />
        <div className="my-2 ml-4">
          <h2
            dangerouslySetInnerHTML={createMarkup(title)}
            className="text-2xl interaction-underline"
          />
          <p className="text-sm uppercase">{ARTISTS_MAP[category_id]}</p>
        </div>
      </a>
    </li>
  )
}

export default ResultsListItem
