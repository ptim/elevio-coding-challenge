import React from 'react'

const Err = ({ error }) => {
  console.error(error)
  return (
    <div className="p-4 bg-red-300 text-red-900 text-center">
      Oh no!{' '}
      <span role="img" aria-label="Terrified cat emoji">
        🙀
      </span>{' '}
      {error.message}
    </div>
  )
}

export default Err
