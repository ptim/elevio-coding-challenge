import React from 'react'

import ResultsListItem from './ResultsListItem'

const ResultsList = ({ response }) => {
  const { results, count, totalResults } = response

  return (
    <div>
      <p className="mb-3 align-right">
        Showing <b>{count}</b> / <b>{totalResults}</b> results
      </p>
      <ul>
        {results.map(r => (
          <ResultsListItem key={r.id} result={r} />
        ))}
      </ul>
    </div>
  )
}

export default ResultsList
