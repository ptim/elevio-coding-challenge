import React from 'react'

import DefaultIntro from './DefaultIntro'

const ResultsEmpty = ({ term, dispatch }) => {
  return (
    <>
      <h2 className="text-lg mb-6">
        Sorry, we didn't find any lyrics matching{' '}
        <b className="font-bold">"{term}"</b>{' '}
        <span role="img" aria-label="Worried face emoji">
          😧
        </span>
      </h2>

      <DefaultIntro
        dispatch={dispatch}
        teaserText={`
          This site is mainly about moody folk;
          try getting into the heads of our
          favourite artists:
        `}
      />

      <p>
        Or you can{' '}
        <a
          href="https://ptim.eleviodocs-staging.com/en"
          //target="_blank"
          //rel="noreferrer noopener"
        >
          browse all lyrics
        </a>
        .
      </p>
    </>
  )
}

export default ResultsEmpty
