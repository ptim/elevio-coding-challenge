import React from 'react'

import DefaultIntro from './DefaultIntro'
import Err from './Err'
import Loading from './Loading'
import ResultsList from './ResultsList'
import ResultsEmpty from './ResultsEmpty'

const SearchResults = ({ state, dispatch }) => {
  const { error, hasSubmitted, isLoading, response, term } = state

  if (error) return <Err error={error} />
  if (isLoading) return <Loading />
  if (!hasSubmitted) return <DefaultIntro dispatch={dispatch} />

  if (!response.count) return <ResultsEmpty dispatch={dispatch} term={term} />
  return <ResultsList response={response} />
}

export default SearchResults
