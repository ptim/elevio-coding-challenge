import React, { useReducer } from 'react'

import reducer, { initialState } from '../utils/reducer'
import useSearchApi from '../utils/useSearchApi'
import useSearchOnLoad from '../utils/useSearchOnLoad'
import SearchForm from './SearchForm'
import SearchResults from './SearchResults'
import { actionCreators } from '../utils/reducer'

import { ReactComponent as Logo } from '../assets/logo.svg'
import backgroundImage from '../assets/rough-background.jpg'

const { TERM_RESET } = actionCreators

let dummyState
// dummyState = require('../dummyData').dummyState

const handleReset = dispatch => event => {
  event.preventDefault()
  dispatch({ type: TERM_RESET })
  // update history so that the user can use native navigation
  window.history.pushState({}, document.title, '/')
}

function App() {
  let [state, dispatch] = useReducer(reducer, initialState)
  useSearchApi(state, dispatch)
  useSearchOnLoad(dispatch)

  return (
    <div
      className="
        flex flex-col
        min-h-screen max-w-md mx-auto
        bg-white bg-cover
      "
      style={{
        backgroundImage: `url(${backgroundImage})`,
      }}
    >
      <header className="my-0 py-12">
        <a
          href="/"
          className="block mx-auto px-4 text-gray-700"
          onClick={handleReset(dispatch)}
        >
          <h1>
            <Logo title="Moody Lyric Search" className="w-full" />
          </h1>
        </a>
      </header>

      <div className="flex-grow px-4">
        <main>
          <SearchForm state={dummyState || state} dispatch={dispatch} />
          <SearchResults state={dummyState || state} dispatch={dispatch} />
        </main>
      </div>
    </div>
  )
}

export default App
