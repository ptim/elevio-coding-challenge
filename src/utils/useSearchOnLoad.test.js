import { createDispatchIfLocationSearch } from './useSearchOnLoad'
import { actionCreators } from './reducer'

describe('createDispatchIfLocationSearch', () => {
  let dispatchSpy

  beforeEach(() => {
    dispatchSpy = jest.fn()
  })

  afterEach(() => {
    jest.clearAllMocks()
  })

  it('should return a function', () => {
    expect(typeof createDispatchIfLocationSearch(dispatchSpy)).toBe(
      'function'
    )
  })

  // JSDOM: 'Error: Not implemented: navigation (except hash changes)'
  // CRA: 'Out of the box, Create React App only supports overriding these Jest options: ...'
  // TODO: (out of scope): eject so we can customise testEnvironment
  // https://github.com/facebook/jest/issues/5124
  it.skip('should dispatch if `?search` is found', () => {
    window.location.search = `?search=${encodeURIComponent('search term')}`
    expect(createDispatchIfLocationSearch(dispatchSpy)()).toHaveBeenCalledWith({
      type: actionCreators.SEARCH_LOADED,
      payload: encodeURIComponent('search term'),
    })
  })
})
