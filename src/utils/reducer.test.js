import reducer, { actionCreators, initialState } from './reducer'
const {
  TERM_RESET,
  TERM_SUBMITTED,
  TERM_UPDATED,
  SEARCH_LOADED,
} = actionCreators

describe('reducer', () => {
  it('throws if an actionCreator is unrecognized', () => {
    expect(() => {
      reducer(initialState, { type: 'UNKNOWN' })
    }).toThrow()
  })

  describe('TERM_RESET', () => {
    it('sets the term to ""', () => {
      const prevState = {
        ...initialState,
        term: 'previous search term',
      }
      expect(reducer(prevState, { type: TERM_RESET })).toEqual(initialState)
    })
  })

  describe('TERM_SUBMITTED', () => {
    it('sets the term and isSubmitting to true', () => {
      const newTerm = 'new term'
      expect(reducer(initialState, { type: TERM_SUBMITTED, payload: newTerm })).toEqual({
        ...initialState,
        term: newTerm,
        isSubmitting: true,
      })
    })
  })

  describe('TERM_UPDATED', () => {
    it('sets the term to `action.payload`', () => {
      const newSearch = 'new search'
      expect(
        reducer(initialState, { type: TERM_UPDATED, payload: newSearch })
      ).toEqual({ ...initialState, term: newSearch })
    })
  })

  describe('SEARCH_LOADED', () => {
    it('sets term and isSubmitting', () => {
      const newSearch = 'new search'
      expect(
        reducer(initialState, { type: SEARCH_LOADED, payload: newSearch })
      ).toEqual({ ...initialState, term: newSearch, isSubmitting: true })
    })
  })
})
