import { useEffect } from 'react'
import axios from 'axios'

import { API_KEY } from '../const'
import { actionCreators } from '../utils/reducer'
const { SEARCH_REQUESTED, SEARCH_RESOLVED, SEARCH_ERRORED } = actionCreators


const useSearchApi = (state, dispatch) => {
  const { isSubmitting, term } = state

  useEffect(() => {
    let didCancel

    async function fetchSearchResults() {
      try {
        dispatch({ type: SEARCH_REQUESTED })
        // update history so that the user can user native navigation
        window.history.pushState({}, document.title, `?search=${term}`)

        const response = await axios.get(
          'https://api.elevio-staging.com/v1/search/en',
          {
            params: {
              url: encodeURIComponent(
                'http://localhost:3000'
              ),
              query: encodeURIComponent(term),
              page: 1,
              rows: 6, // "deal with limiting the number of results displayed" ><
            },
            headers: {
              'x-api-key': API_KEY,
            },
          }
        )

        if (didCancel) return
        dispatch({ type: SEARCH_RESOLVED, payload: response.data })

        window.dataLayer = window.dataLayer || []
        window.dataLayer.push({
          event: 'search_submit',
          term,
        })
      } catch (err) {
        if (didCancel) return
        dispatch({ type: SEARCH_ERRORED, payload: err })
      }
    }
    // XXX: this is messy (prefer from validation), but we need to prevent a fetch on mount
    if (term.length >= 3) fetchSearchResults()

    return () => {
      didCancel = true
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isSubmitting]) // only refresh this effect when the form is submitting!

  return undefined
}

export default useSearchApi
