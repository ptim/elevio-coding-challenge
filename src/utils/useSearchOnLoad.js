import { useEffect } from 'react'
import { actionCreators } from './reducer'

export const createDispatchIfLocationSearch = dispatch => () => {
  const match = window.location.search.match(/search=([^&]+)/)
  if (match && match[1]) {
    dispatch({
      type: actionCreators.SEARCH_LOADED,
      payload: decodeURIComponent(match[1]),
    })
  }
}

const useSearchOnLoad = dispatch => {
  useEffect(createDispatchIfLocationSearch(dispatch), [])
}

export default useSearchOnLoad
