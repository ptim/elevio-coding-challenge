const defaultResponse = {
  count: 0,
  currentPage: 0,
  results: [],
  totalPages: 0,
  totalResults: 0,
}

export const initialState = {
  error: null,
  isSubmitting: false,
  hasSubmitted: false,
  isLoading: false,
  response: defaultResponse,
  term: '',
}

const SEARCH_ERRORED = 'SEARCH:ERRORED'
const SEARCH_LOADED = 'SEARCH:LOADED'
const SEARCH_REQUESTED = 'SEARCH:REQUESTED'
const SEARCH_RESOLVED = 'SEARCH:RESOLVED'
const TERM_SUBMITTED = 'TERM:SUBMITTED'
const TERM_RESET = 'TERM:RESET'
const TERM_UPDATED = 'TERM:UPDATED'

export const actionCreators = {
  SEARCH_ERRORED,
  SEARCH_LOADED,
  SEARCH_REQUESTED,
  SEARCH_RESOLVED,
  TERM_SUBMITTED,
  TERM_RESET,
  TERM_UPDATED,
}

function reducer(state, action) {
  // console.log({...action});

  switch (action.type) {
    case TERM_SUBMITTED:
      return {
        ...state,
        isSubmitting: true,
        term: action.payload,
      }

    case TERM_RESET:
      return initialState

    case TERM_UPDATED:
      return {
        ...state,
        term: action.payload,
      }

    case SEARCH_REQUESTED:
      return {
        ...state,
        error: null,
        isLoading: true,
        isSubmitting: false,
        hasSubmitted: true,
      }

    case SEARCH_RESOLVED:
      return {
        ...state,
        isLoading: false,
        response: action.payload,
      }

    case SEARCH_ERRORED:
      return {
        ...state,
        error: action.payload,
        isLoading: false,
        response: defaultResponse,
      }

    case SEARCH_LOADED:
      return {
        ...state,
        term: action.payload,
        isSubmitting: true,
      }

    default:
      throw new Error('actionCreator unrecognised')
  }
}

export default reducer
