// https://jestjs.io/docs/en/configuration.html#testenvironment-string
const JsDomEnvironment = require('jest-environment-jsdom')

let _windowLocation

class CustomEnvironment extends JsDomEnvironment {
  constructor(config, context) {
    super(config, context)
    this.testPath = context.testPath
    this.docblockPragmas = context.docblockPragmas
  }

  async setup() {
    await super.setup()
    // extend window.location to overcome:
    // JSDOM: 'Error: Not implemented: navigation (except hash changes)'
    _windowLocation = window.location
    window.location = {
      href: '',
      search: '',
    }
  }

  async teardown() {
    // revert
    window.location = _windowLocation
    await super.teardown()
  }

  runScript(script) {
    return super.runScript(script)
  }
}

module.exports = CustomEnvironment
